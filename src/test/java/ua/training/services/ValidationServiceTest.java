//package ua.training.controller;
//
//import junit.framework.Assert;
//import org.junit.Test;
//import ua.training.model.domain.Groups;
//import ua.training.services.Field;
//import ua.training.services.ValidationService;
//
//import java.util.Map;
//
//public class ValidationServiceTest {
//    private ValidationService service;
//
//    @Test
//    public void testCheckCorrectAnswer() throws Exception {
//        service = new ValidationService();
//        Field field = new Field();
//        String fieldname = "Date of birth";
//        String dateToSave = "1990-06-17";
//        field.setFieldName(fieldname);
//        field.setRegEx("^((19|20)\\d\\d)-(0?[1-9]|1[012])-[0-3]\\d$");
//        String actualRecord = "";
//        service.checkAnswer(dateToSave, field);
//        for (Map.Entry<String, String> entry : service.getQuestionnaire().entrySet()) {
//            if (entry.getKey().equals(fieldname)) {
//                actualRecord = entry.getValue();
//            }
//        }
//        Assert.assertEquals(dateToSave, actualRecord);
//    }
//
//    @Test
//    public void testCheckWrongAnswer() throws Exception {
//        service = new ValidationService();
//        Field field = new Field();
//        String fieldname = "Date of birth";
//        String dateToSave = "9999-99-99";
//        field.setFieldName(fieldname);
//        field.setRegEx("^((19|20)\\d\\d)-(0?[1-9]|1[012])-[0-3]\\d$");
//        service.checkAnswer(dateToSave, field);
//        Assert.assertTrue(service.getQuestionnaire().isEmpty());
//    }
//
//    @Test
//    public void testCheckMenuWithEnumValue() throws Exception {
//        service = new ValidationService();
//        Field field = new Field();
//        String fieldname = "group";
//        String group = "services";
//        field.setFieldName(fieldname);
//        service.checkMenu(group, field);
//        String expectedRecord = Groups.Services.toString();
//        String actualRecord ="";
//        for (Map.Entry<String, String> entry : service.getQuestionnaire().entrySet()) {
//            if (entry.getKey().equals(fieldname)) {
//                actualRecord = entry.getValue();
//            }
//        }
//        Assert.assertEquals(expectedRecord, actualRecord);
//    }
//
//    @Test
//    public void testCheckMenuWithIncorrectValue() throws Exception {
//        service = new ValidationService();
//        Field field = new Field();
//        String fieldname = "group";
//        String group = "4";
//        field.setFieldName(fieldname);
//        service.checkMenu(group, field);
//        Assert.assertTrue(service.getQuestionnaire().isEmpty());
//    }
//
//    }
