package ua.training.services;

import ua.training.Exceptions.NonUniqueLoginException;
import ua.training.model.dao.NoteDao;
import ua.training.model.dao.NoteJDBCManager;
import ua.training.model.domain.Address;
import ua.training.model.domain.Note;
import ua.training.view.View;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

public class RecordService {
    private View view;
    private HashMap<String, String> questionnaire;
    private NoteDao noteDao;
    private Note note;

    public RecordService() {
        noteDao = new NoteJDBCManager();
    }

    public void setValidatedData(HashMap<String, String> questionnaire) {
        this.questionnaire= questionnaire;
        note = convertUsersDataToModel(questionnaire);
    }

    public void saveNote() throws NonUniqueLoginException {
       try {
           noteDao.create(note);
       } catch (SQLException e) {
           System.err.print(e.getMessage());
       }
    }

    public void addNewLogin(String login) {
        note.setLogin(login);
    }

    private Note convertUsersDataToModel(HashMap<String, String> questionnaire) {
        note = new Note();
        this.questionnaire = questionnaire;
        for (Map.Entry<String, String> entry : questionnaire.entrySet()) {
            if (entry.getKey().equals("name")) {
                note.setName(entry.getValue());
            }
            if (entry.getKey().equals("surname")) {
                note.setSurname(entry.getValue());
            }
            if (entry.getKey().equals("middle name")) {
                note.setMiddleName(entry.getValue());
            }
            if (entry.getKey().equals("login")) {
                note.setLogin(entry.getValue());
            }
            if (entry.getKey().equals("comment")) {
                note.setComment(entry.getValue());
            }
            if (entry.getKey().equals("group")) {
                note.setGroup(entry.getValue());
            }
            if (entry.getKey().equals("phone number (home)")) {
                note.setPhoneNumberHome(entry.getValue());
            }
            if (entry.getKey().equals("cell phone number")) {
                note.setCellPhoneNumber(entry.getValue());
            }
            if (entry.getKey().equals("cell phone number (optional)")) {
                note.setCellPhoneNumberOptional(entry.getValue());
            }
            if (entry.getKey().equals("email")) {
                note.setEmail(entry.getValue());
            }
            if (entry.getKey().equals("skype")) {
                note.setSkype(entry.getValue());
            }
            note.setAddress(convertAddressToModel());
            LocalDate currentDate = LocalDate.now();
            note.setDateNoteAdded(currentDate);
        }
        return note;
    }

    private Address convertAddressToModel() {
        Address address = new Address();
        for (Map.Entry<String, String> entry : questionnaire.entrySet()) {
            if (entry.getKey().equals("address index")) {
                address.setAddressIndex(entry.getValue());
            }
            if (entry.getKey().equals("City")) {
                address.setAddressCity(entry.getValue());
            }
            if (entry.getKey().equals("Street")) {
                address.setAddressStreet(entry.getValue());
            }
            if (entry.getKey().equals("House number")) {
                address.setAddressHouseNumber(entry.getValue());
            }
            if (entry.getKey().equals("Apartment number")) {
                address.setAddressAppartment(entry.getValue());
            }
        }
        return address;
    }

    public String getFullName() {
        String name = "";
        String surname = "";
        for (Map.Entry<String, String> entry : questionnaire.entrySet()) {
            if (entry.getKey().equals("name")) {
                name = entry.getValue();
            }
            if (entry.getKey().equals("surname")) {
                surname = entry.getValue();
            }
        }
        return view.concatString(surname, " ", name.trim().substring(0,0), ".");
    }

    public String getUserAddress() {
        Address address = convertAddressToModel();
        return view.concatString(address.getAddressStreet(), address.getAddressHouseNumber(),
                address.getAddressAppartment(), address.getAddressCity(), address.getAddressIndex());
    }

}
