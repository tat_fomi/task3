package ua.training.model.domain;

import java.time.LocalDate;

public class Note {
    private String surname;
    private String name;
    private String middleName;
    private String login;
    private String comment;
    private String group;
    private String phoneNumberHome;
    private String cellPhoneNumber;
    private String cellPhoneNumberOptional;
    private String email;
    private String skype;
    private LocalDate dateNoteAdded;
    private LocalDate dateNoteChanged;
    private Address address;

    public Note() {

    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) { this.middleName = middleName; }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getPhoneNumberHome() {
        return phoneNumberHome;
    }

    public void setPhoneNumberHome(String phoneNumberHome) {
        this.phoneNumberHome = phoneNumberHome;
    }

    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public void setCellPhoneNumber(String cellPhoneNumber) {
        this.cellPhoneNumber = cellPhoneNumber;
    }

    public String getCellPhoneNumberOptional() {
        return cellPhoneNumberOptional;
    }

    public void setCellPhoneNumberOptional(String cellPhoneNumberOptional) {
        this.cellPhoneNumberOptional = cellPhoneNumberOptional;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public LocalDate getDateNoteAdded() {
        return dateNoteAdded;
    }

    public void setDateNoteAdded(LocalDate dateNoteAdded) {
        this.dateNoteAdded = dateNoteAdded;
    }

    public LocalDate getDateNoteChanged() {
        return dateNoteChanged;
    }

    public void setDateNoteChanged(LocalDate dateNoteChanged) {
        this.dateNoteChanged = dateNoteChanged;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
