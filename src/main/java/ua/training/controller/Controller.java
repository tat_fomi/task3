package ua.training.controller;

import ua.training.Exceptions.NonUniqueLoginException;
import ua.training.services.RecordService;
import ua.training.services.ValidationService;
import ua.training.view.View;

public class Controller {
    private ValidationService validationService;
    private RecordService recordService;
    private View view;
    private boolean isUserDataSaved;

    public Controller() {
        view = new View();
        validationService = new ValidationService();
        recordService = new RecordService();
    }

    public void processUser() {
        view.printGreeting();
        validationService.askQuestions();
        recordService.setValidatedData(validationService.getQuestionnaire());
        while (!isUserDataSaved) {
            saveUserData();
        }
    }

    public void saveUserData() {
        try {
            recordService.saveNote();
            isUserDataSaved = true;
        } catch (NonUniqueLoginException exception) {
            view.askForNewLogin();
            recordService.addNewLogin(validationService.checkNewLogin());
        }
    }
    }
