package ua.training.services;

import javax.xml.bind.annotation.XmlElement;

class Field {
    private String fieldName;
    private String regEx;
    private String error;
    private boolean isValidated;

    String getFieldName() {
        return fieldName;
    }
    @XmlElement(name="FieldName")
    void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    String getRegEx() {
        return regEx;
    }

    @XmlElement(name="RegExp")
    void setRegEx(String regEx) {
        this.regEx = regEx;
    }

    String getErrorType() {
        return error;
    }

    @XmlElement(name="ErrorMessage")
    void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "[fieldName= " + fieldName + " regEx= " + regEx + " error: " + error + "]\n";
    }

    boolean isValidated() {
        return isValidated;
    }

    void setValidated(boolean validated) {
        isValidated = validated;
    }
}
