package ua.training.model.domain;

public class Address{
    private String addressIndex;
    private String addressCity;
    private String addressStreet;
    private String addressHouseNumber;
    private String addressAppartment;

    public String getAddressIndex() {
        return addressIndex;
    }

    public void setAddressIndex(String addressIndex) {
        this.addressIndex = addressIndex;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public String getAddressHouseNumber() {
        return addressHouseNumber;
    }

    public void setAddressHouseNumber(String addressHouseNumber) {
        this.addressHouseNumber = addressHouseNumber;
    }

    public String getAddressAppartment() {
        return addressAppartment;
    }

    public void setAddressAppartment(String addressAppartment) {
        this.addressAppartment = addressAppartment;
    }
}
