package ua.training.view;

/**
 * Created by Tetianka on 20.04.2017.
 */
public class View {
    private String greeting = "Welcome! You can add new note to the notebook";
    private String suggestion = "Please enter ";
    private String error = "Wrong input: ";

    public String concatString(String ... args) {
        StringBuffer message = new StringBuffer();
        for (String word : args) {
            message.append(word);
            message.append(" ");
        }
        return message.toString();
    }

    public void printGreeting() {
        System.out.println("");printMessage(greeting);
    }

    public void askForNewLogin() {
        printMessage("This login is already taken. Please enter new login");
    }

    public void printSuggestion() {
        printMessage(suggestion);
    }

    public void printError() {
        printMessage(error);
    }

    public void printMessage(String message) {
        System.out.println(message);
    }

    public String getGreeting() {
        return greeting;
    }

    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }

    public String getSuggestion() {
        return suggestion;
    }

    public void setSuggestion(String suggestion) {
        this.suggestion = suggestion;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
