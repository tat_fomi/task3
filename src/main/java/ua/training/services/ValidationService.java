package ua.training.services;

import ua.training.model.domain.Address;
import ua.training.model.domain.Groups;
import ua.training.model.domain.Note;
import ua.training.view.View;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class ValidationService {
    private View view;
    private Note note;
    /**
     * FieldValidator is a list of fields, it is parsed from FieldValidation.xml, where fields ans their regular expressions
     * and error types are defined
     */
    private FieldValidator validator;
    private HashMap<String, String> questionnaire;

    public ValidationService() {
        view = new View();
        note = new Note();
        questionnaire = new HashMap<>(20);
    }

    /**
     * Gets through the list of Fields until all fields are validated
     * @throws Exception
     */
    public void askQuestions() {
        try {
            validator = getFieldValidator();
        } catch (JAXBException e) {
            System.err.print(e);
        }
        LinkedList<Field> fields = validator.getFields();
       for (int i = 0; i < fields.size(); i++) {
           Field currentField = fields.get(i);
           while (!currentField.isValidated()) {
               askOneQuestion(currentField);
           }
       }
    }

    private FieldValidator getFieldValidator() throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(FieldValidator.class);
        Unmarshaller unMarshaller = context.createUnmarshaller();
        File file = new File("src\\resources\\FieldValidation.xml");
        return (FieldValidator) unMarshaller.unmarshal(file);
    }

    private void askOneQuestion(Field field) {
       view.printMessage(view.concatString(view.getSuggestion(), field.getFieldName()));
       Scanner scan = new Scanner(System.in);
       String textToCheck;
       if (field.getFieldName().equals("group")) {
           view.printMessage(view.concatString("1 " , Groups.Colleagues.toString() , "\n2 " , Groups.Friends.toString(),
                   "\n3 " , Groups.Services.toString()));
           textToCheck = scan.nextLine();
           checkMenu(textToCheck, field);
       } else {
           textToCheck = scan.nextLine();
           checkAnswer(textToCheck, field);
       }
   }

   public String checkNewLogin() {
       Scanner scan = new Scanner(System.in);
       Field login = null;
       LinkedList<Field> fields = validator.getFields();
       for (int i = 0; i < fields.size(); i++) {
           if (fields.get(i).getFieldName().equals("login")) {
               login = fields.get(i);
           }
       }
       String textToCheck = scan.next();
       while (!login.isValidated()) {
           checkAnswer(textToCheck, login);
       }
       return textToCheck;
   }

   public void checkAnswer(String textToCheck, Field field) {
       Pattern pattern = Pattern.compile(field.getRegEx());
       Matcher matcher = pattern.matcher(textToCheck);
       if (matcher.find()) {
           addCorrectData(matcher.group(), field);
       } else {
           if (field.getFieldName().equals("cell phone number (optional)") && textToCheck.isEmpty()) {
               addCorrectData(null, field);
           } else {
               view.printMessage(view.concatString(view.getError(), field.getErrorType()));
           }
       }
   }
    /**
     * Is used to check fields with Enum as in console version the only implementation is one with menu.
     */
    public void checkMenu(String textToCheck, Field field) {
        if(textToCheck.equalsIgnoreCase("1") || textToCheck.equalsIgnoreCase("colleagues")){
            addCorrectData(Groups.Colleagues.toString(), field);
        }else if(textToCheck.equalsIgnoreCase("2") || textToCheck.equalsIgnoreCase("friends")){
            addCorrectData(Groups.Friends.toString(), field);
        }else if(textToCheck.equalsIgnoreCase("3") || textToCheck.equalsIgnoreCase("services")){
            addCorrectData(Groups.Services.toString(), field);
        }
    }

    private void addCorrectData(String userData, Field field) {
        questionnaire.put(field.getFieldName(), userData);
        field.setValidated(true);
   }

    public HashMap<String, String> getQuestionnaire() {
        return questionnaire;
    }

}


