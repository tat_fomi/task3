package ua.training.model.dao;

import ua.training.Exceptions.NonUniqueLoginException;
import ua.training.model.domain.Note;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class NoteJDBCManager implements NoteDao {

    @Override
    public void create(Note note) throws SQLException, NonUniqueLoginException {
        try {
            Connection connection = createConnection();
            if (isUniqueLogin(note.getLogin())) {
                Statement statement = connection.createStatement();
                String query = String.format("INSERT INTO NOTE VALUES ('%1$s', '%2$s', '%3$s', '%4$s', '%5$s', '%6$s', '%7$s'," +
                                "'%8$s', TO_DATE('%9$s', 'yyyy/mm/dd'), TO_DATE('%10$s', 'yyyy/mm/dd'))",
                        note.getName(), note.getSurname(), note.getLogin(), note.getComment(), note.getGroup(),
                        note.getCellPhoneNumber(), note.getCellPhoneNumber(), note.getCellPhoneNumberOptional(),
                        note.getDateNoteAdded().toString(), note.getDateNoteChanged().toString());
                statement.executeUpdate(query);
                statement.close();
                connection.close();
            } else {
                throw new NonUniqueLoginException();
            }
        } catch (SQLException e ) {
            e.printStackTrace();
        }
    }

    private boolean isUniqueLogin(String login) throws SQLException {
        List<String> logins = findLogins();
        String loginFound = logins.stream()
                .filter(e -> login.equals(e))
                .findAny()
                .orElse(null);
        return (loginFound == null);
    }

    private List<String> findLogins() throws SQLException {
        ArrayList<String> logins = new ArrayList<>();
        Connection connection = createConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT LOGIN FROM NOTE");
        while (resultSet.next()) {
            logins.add(resultSet.getString(1));
        }
        statement.close();
        connection.close();
        return logins.isEmpty() ? null : logins;
    }

    @Override
    public List findAll() {
        return null;
    }

    @Override
    public Note readByLogin(String login) {
        return null;
    }

    @Override
    public Note read(Long id) {
        return null;
    }

    @Override
    public boolean update(Note note) {
        return false;
    }

    @Override
    public boolean delete(Note note) {
        return false;
    }

    private Connection createConnection() {
        Locale.setDefault(Locale.ENGLISH);
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = null;
        String url = "jdbc:oracle:thin:@localhost:1521:XE";
        try {
            connection = DriverManager.getConnection(url, "tat", "eCo2187975");
        } catch (SQLException sqe) {
            sqe.printStackTrace();
        }
        return connection;
    }
}
