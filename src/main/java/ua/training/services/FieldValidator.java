package ua.training.services;
import javax.xml.bind.annotation.*;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.LinkedList;
import java.util.List;

@XmlRootElement (name = "ListOfFields")
 class FieldValidator {
   private LinkedList <Field> listOfFields;

    public FieldValidator() {

    }

    public FieldValidator(List<Field> list) {

    }

    @XmlElement(name = "fieldValidator")
    public LinkedList<Field> getFields() {
        if(listOfFields == null) {
            listOfFields = new LinkedList<Field>();
        }
        return listOfFields;
    }

    public void setListOfFields(LinkedList<Field> list) {
        listOfFields = list;
    }

    @Override
    public String toString() {
        return "FieldValidator list:" + listOfFields;
    }

}
