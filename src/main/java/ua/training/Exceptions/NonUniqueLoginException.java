package ua.training.Exceptions;

public class NonUniqueLoginException extends Exception {

    public NonUniqueLoginException() {
    }

    public NonUniqueLoginException(String message, Throwable cause) {
        super(message, cause);
    }

    public NonUniqueLoginException(String message) {
        super(message);
    }

    public NonUniqueLoginException(Throwable cause) {
        super(cause);
    }
}
