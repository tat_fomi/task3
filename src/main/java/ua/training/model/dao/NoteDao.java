package ua.training.model.dao;

import ua.training.Exceptions.NonUniqueLoginException;
import ua.training.model.domain.Note;

import java.sql.SQLException;
import java.util.List;

public interface NoteDao {
    void create(Note note) throws SQLException, NonUniqueLoginException;
    List findAll();
    public Note readByLogin(String login);
    Note read(Long id);
    boolean update(Note note);
    boolean delete(Note note);
}
